// Constitue le plateau de base

export const boards = () => {
    let board = [];
    let n = 0;
    for(let i = 0; i < 64; i++){
        if(i % 2 === 0 || i === 0) {
            board.push(null);
        } else if (i > 23 && i < 39) {
            board.push(null);
        } else {
            board.push(n);
            n++;
        }
    }
    return board;
}