/* Permet de construire la structure HTML de notre boards*/ 

export const boardsHtml = () => {
    let n = 0;
    let board = '';
    for(let i = 0; i < 8; i++) {
        board += '<tr>';
        for(let j = 0; j < 8; j++) {
            if(i % 2 === 0) {
                if(j === 0 || j % 2 === 0) {
                    board += '<td class=\'emptyTd\'></td>';
                } else {
                    if(i <= 2) {
                        board += '<td><p class=\'redP\' id=\'' + n + '\'></p></td>';
                        n++;
                    } else if (i >= 5) {
                        board += '<td><p class=\'blackP\' id=\'' + n + '\'></td>';
                        n++;
                    } else {
                        board +="<td></td>"
                    }
                }
            } else {
                if(j === 0 || j % 2 === 0) {
                    if (i >= 5) {
                        board += '<td><p class=\'blackP\' id=\'' + n + '\'></td>';
                        n++;
                    } else if (i <= 2) {
                        board += '<td><p class=\'redP\' id=\'' + n + '\'></p></td>';
                        n++;
                    } else {
                        board += '<td></td>';
                    }
                } else {
                    board += '<td class=\'emptyTd\'></td>';
                }
            }
        }
        board += '</tr>'
    }
    return board;
}