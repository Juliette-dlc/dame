import { boardsHtml } from './components/boards_html.js';
import { boards } from './components/boards.js';

window.onload = (event) => {
    document.getElementById("board").innerHTML = boardsHtml();
    boards();
}